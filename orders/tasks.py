from celery import shared_task
from django.db import transaction

from orders.models import Order


@shared_task
def process_order(order_pk):
    """
    Assigns workers for each service, adds order common price, changes order
    status.
    """
    with transaction.atomic():
        order = Order.objects.get(pk=order_pk)
        order.assign_workers()
        order.price = order.services.get_common_price()
        order.status = Order.STATUS_PROCESSED
        order.save()
