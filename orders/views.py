from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView

from orders.forms import OrderForm
from orders.mixins import OrderResponseMixin
from orders.models import Order


class OrderCreateView(LoginRequiredMixin, OrderResponseMixin, CreateView):
    model = Order
    form_class = OrderForm
    success_url = reverse_lazy('orders:create')
