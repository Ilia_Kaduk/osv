from random import choice

from django.contrib.auth.models import User
from django.db import models
from django.contrib.postgres.aggregates import ArrayAgg

from services.models import Service


class Order(models.Model):
    STATUS_PROCESSING = 'processing'
    STATUS_PROCESSED = 'processed'
    STATUS_DONE = 'done'
    STATUS_CANCELED = 'canceled'

    customer = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='personal_orders'
    )
    services = models.ManyToManyField(Service, through='OrderServiceWorker')
    workers = models.ManyToManyField(
        User,
        through='OrderServiceWorker',
        related_name='work_orders'
    )
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True
    )
    status = models.CharField(max_length=50, default=STATUS_PROCESSING)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def assign_workers(self):
        """Assigns random workers to each service."""
        workers = {
            service['pk']: choice(service['worker_list'])
            for service in self.services.annotate(
                worker_list=ArrayAgg('workers__pk')
            ).values('pk', 'worker_list')
        }

        order_service_worker_list = []
        orders_services_workers = self.order_service_worker.all()
        for order_service_worker in orders_services_workers:
            worker_id = workers[order_service_worker.service_id]
            order_service_worker.worker_id = worker_id
            if order_service_worker.worker_id:
                order_service_worker.status = \
                    OrderServiceWorker.STATUS_ASSIGNED
            order_service_worker_list.append(order_service_worker)

        return self.order_service_worker.bulk_update(
            order_service_worker_list,
            ['worker', 'status']
        )


class OrderServiceWorker(models.Model):
    STATUS_PROCESSING = 'processing'
    STATUS_ASSIGNED = 'assigned'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_DONE = 'done'
    STATUS_CANCELED = 'canceled'

    class Meta:
        db_table = 'orders_order_service_worker'

    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='order_service_worker'
    )
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    worker = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    status = models.CharField(max_length=50, default=STATUS_PROCESSING)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
